module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
    jest: true,
  },
  parser: 'babel-eslint',
  plugins: ['react'],
  extends: [
    'airbnb',
    'prettier',
    'prettier/react',
    'plugin:prettier/recommended',
  ],
  rules: {
    'prettier/prettier': 'error',
    'import/no-cycle': 0,
    'react/jsx-props-no-spreading': 'off',
    'react/prop-types': [2, { ignore: ['theme'] }],
    'import/no-extraneous-dependencies': 0,
    'no-plusplus': 0,
  },
  globals: {
    window: true,
    fetch: false,
    document: true,
  },
  settings: {
    'import/resolver': {
      node: {
        paths: ['.'],
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
    },
  },
};
