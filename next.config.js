const withOffline = require('next-offline');

const appConfig = require('./config/app');
const serverConfig = require('./config/server');
const pwaConfig = require('./config/pwa');

const config = {
  distDir: 'dist',
  publicRuntimeConfig: { ...appConfig },
  serverRuntimeConfig: { ...serverConfig },
  workboxOpts: { ...pwaConfig },
};

module.exports = withOffline(config);
