import React from 'react';
import Link from 'next/link';
import { useDispatch } from 'react-redux';

import { Page, Title } from '@components';
import { getStars } from '@services/user';

function HomePage(props) {
  const dispatch = useDispatch();
  return (
    <Page title="Home">
      <Title> Home Page </Title>
      <button onClick={() => dispatch(getStars())}>
        Click to see the stars
      </button>
      <Link href="/about">
        <a>Sobre</a>
      </Link>
    </Page>
  );
}

export default HomePage;
