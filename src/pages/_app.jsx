import React from 'react';
import App from 'next/app';
import Head from 'next/head';

import { Provider as ReduxProvider } from 'react-redux';
import { ThemeProvider } from 'styled-components';

import withRedux from '@state';

const theme = {
  colors: {
    primary: '#0070f3',
  },
};

const AppHead = () => (
  <>
    <Head>
      <title>App NextJS</title>
      <meta charSet="UTF-8" key="charset" />
      <meta name="theme-color" content="#4285f4" key="themeColor" />
      <link rel="manifest" href="/static/manifest.json" />
      <meta name="description" content="Next POC" key="description" />
      <link rel="apple-touch-icon" href="ios-icon.png" />
      <meta
        name="viewport"
        content="initial-scale=1.0, width=device-width"
        key="viewport"
      />
    </Head>
  </>
);

class Application extends App {
  render() {
    const { Component, pageProps, store } = this.props;
    return (
      <>
        <AppHead />
        <ReduxProvider store={store}>
          <ThemeProvider theme={theme}>
            <Component {...pageProps} />
          </ThemeProvider>
        </ReduxProvider>
      </>
    );
  }
}

export default withRedux(Application);
