import React from 'react';
import Link from 'next/link';

import { Page, Title } from '@components';

function AboutPage() {
  return (
    <Page title="Sobre">
      <Title>About</Title>
      <Link href="/home">
        <a>Home</a>
      </Link>
    </Page>
  );
}

export default AboutPage;
