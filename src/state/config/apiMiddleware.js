import apiClient from '@libs/api';
import { actions as appActions } from '@state/modules/app';
import { actions as apiActions } from '@state/modules/api';

const { APP_LOADING_SHOW, APP_LOADING_HIDE, APP_ERROR_SHOW } = appActions;
const { API_REQUEST } = apiActions;

const apiMiddleware = ({ dispatch }) => next => action => {
  next(action);
  if (action.type !== API_REQUEST) return;

  const { url, method, data, headers, onSuccess, onFailure } = action.payload;
  const dataOrParams = ['GET', 'DELETE'].includes(method) ? 'params' : 'data';

  console.log(apiClient.defaults.baseURL);

  dispatch({ type: APP_LOADING_SHOW });
  apiClient
    .request({ url, method, headers, [dataOrParams]: data })
    .then(({ data }) => {
      onSuccess(data);
    })
    .catch(error => {
      dispatch({ type: APP_ERROR_SHOW, state: { errorMessage: error } });
      if (error.response && error.response.status === 403) {
        console.log('Access Denied');
      }
      onFailure(error);
    })
    .finally(() => {
      dispatch({ type: APP_LOADING_HIDE });
    });
};

export default apiMiddleware;
