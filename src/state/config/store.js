import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import apiMiddleware from './apiMiddleware';
import reducers from '../modules';

const initialState = {
  application: {
    loading: false,
    error: false,
  },
};

const initializeStore = (preloadedState = initialState) => {
  return createStore(
    reducers,
    preloadedState,
    composeWithDevTools(applyMiddleware(thunk, apiMiddleware)),
  );
};

export default initializeStore;
