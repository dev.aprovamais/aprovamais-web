import React from 'react';
import initializeStore from './store';
import getPreloadedData from './preload';

const isServer = typeof window === 'undefined';
const reduxKey = '__NEXT_REDUX_STORE__';

function createStore(initialState) {
  if (isServer) {
    return initializeStore(initialState);
  }
  if (!window[reduxKey]) {
    window[reduxKey] = initializeStore(initialState);
  }
  return window[reduxKey];
}

const withRedux = Component =>
  class Redux extends React.Component {
    static async getInitialProps(context) {
      const initialState = await getPreloadedData();
      const reduxStore = createStore(initialState);
      context.ctx.store = reduxStore;

      const appProps = Component.getInitialProps
        ? await Component.getInitialProps(context)
        : {};

      return {
        ...appProps,
        store: reduxStore.getState(),
      };
    }

    constructor(props) {
      super(props);
      this.store = createStore(props.store);
    }

    render() {
      return <Component {...this.props} store={this.store} />;
    }
  };

export default withRedux;
