const preloadedStore = {
  app: {
    loading: false,
    error: false,
  },
};

const getPreloadedData = async () => ({
  ...preloadedStore,
});

export default getPreloadedData;
