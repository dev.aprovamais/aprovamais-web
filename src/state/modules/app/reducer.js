import actions from './actions';

const application = (currentState = {}, action) => {
  const actionDispacher = actions[action.type];
  return actionDispacher ? actionDispacher(currentState, action) : currentState;
};

export default application;
