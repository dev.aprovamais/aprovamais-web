export const APP_LOADING_SHOW = 'app/loading/show';
export const APP_LOADING_HIDE = 'app/loading/hide';
export const APP_ERROR_SHOW = 'app/error/show';
export const APP_ERROR_HIDE = 'app/error/hide';

const actions = {
  [APP_LOADING_SHOW]: state => ({ ...state, loading: true }),
  [APP_LOADING_HIDE]: state => ({ ...state, loading: false }),
  [APP_ERROR_HIDE]: state => ({
    ...state,
    state: { errorMessage: null },
    error: false,
  }),
  [APP_ERROR_SHOW]: (state, action) => ({
    ...state,
    errorMessage: action.state.errorMessage,
    error: true,
  }),
};

export default actions;
