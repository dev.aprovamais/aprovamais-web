import { combineReducers } from 'redux';

// Reducers Mapping
import { reducer as app } from './app';
import { reducer as api } from './api';
import { reducer as user } from './user';

const reducers = combineReducers({
  app,
  api,
  user,
});

export default reducers;
