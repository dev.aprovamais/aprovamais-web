export const API_REQUEST = 'api/request';
export const API_REQUEST_CACHEABLE = 'api/request/cacheable';

const actions = {
  [API_REQUEST]: (state, action) => ({
    ...state,
    ...action.state,
  }),
  [API_REQUEST_CACHEABLE]: (state, action) => ({
    ...state,
    ...action.state,
  }),
};

export default actions;
