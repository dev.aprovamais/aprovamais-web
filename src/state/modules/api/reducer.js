import actions from './actions';

const api = (currentState = {}, action) => {
  const actionDispacher = actions[action.type];
  return actionDispacher ? actionDispacher(currentState, action) : currentState;
};

export default api;
