import actions from './actions';

const user = (currentState = {}, action) => {
  const actionDispacher = actions[action.type];
  return actionDispacher ? actionDispacher(currentState, action) : currentState;
};

export default user;
