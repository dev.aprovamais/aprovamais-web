export const USER_CHANGE = 'user/change';

const actions = {
  [USER_CHANGE]: (state, action) => ({ ...state, ...action.state }),
};

export default actions;
