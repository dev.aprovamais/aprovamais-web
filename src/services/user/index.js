import { apiRequest } from '@libs/api';
import { actions } from '@state/modules/user';

const { USER_CHANGE } = actions;

export const getStars = () => dispatch =>
  dispatch(
    apiRequest({
      method: 'GET',
      url: '/next.js',
      onSuccess: () =>
        dispatch({
          type: USER_CHANGE,
          state: {
            name: 'storo',
          },
        }),
    }),
  );
