const { createServer } = require('http');
const { join } = require('path');
const { parse } = require('url');
const next = require('next');

const app = next({ dev: process.env.NODE_ENV !== 'production' });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  createServer((req, res) => {
    const parsedUrl = parse(req.url, true);
    const { pathname } = parsedUrl;
    const staticFiles = ['/manifest.json', '/service-worker.js'];

    if (staticFiles.indexOf(pathname) > -1) {
      const filePath = join('dist', pathname);
      app.serveStatic(req, res, filePath);
    } else {
      handle(req, res, parsedUrl);
    }
  }).listen(3000, () => {
    console.log(`🚀 started on http://localhost:${3000}`);
  });
});
