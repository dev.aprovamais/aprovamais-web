import Head from 'next/head';

const Page = ({ children, title }) => (
  <>
    <Head>{title && <title>{title}</title>}</Head>
    <main>{children}</main>
  </>
);

export default Page;
