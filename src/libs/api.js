import axios from 'axios';

import config from '@config/app';
import { actions } from '@state/modules/api';

const apiClient = axios.create({
  baseURL: config.api.basePath,
  timeout: 3000,
});

apiClient.defaults.headers.common['Content-Type'] = 'application/json';

const apiRequest = ({
  method = 'GET',
  url,
  headers = [],
  data = null,
  onSuccess = () => {},
  onFailure = () => {},
}) => ({
  type: actions.API_REQUEST,
  payload: { url, method, headers, data, onSuccess, onFailure },
  state: { method, url, headers, data },
});

export { apiClient as default, apiRequest };
