import { withData } from 'next-apollo';
import { HttpLink } from 'apollo-boost';

import appConfig from '@config/app';

const config = {
  link: new HttpLink({
    uri: appConfig.graphql.cms.path,
    opts: {
      credentials: 'same-origin',
    },
  }),
};

export default withData(config);
