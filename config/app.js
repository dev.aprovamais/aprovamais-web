const isProd = process.env.NODE_ENV === 'production';

const appConfig = {
  app: {
    appPath: isProd ? '' : '',
    cdnUrl: isProd ? 'https://cdn.mydomain.com' : '',
    assets: isProd ? '/static/assets' : '/static/assets',
    console: isProd ? false : true,
    logging: isProd ? true : false,
  },
  api: {
    basePath: isProd
      ? 'https://api.github.com/repos/victormath12'
      : 'https://api.github.com/repos/zeit',
  },
  graphql: {
    bff: {
      path: isProd
        ? 'https://api.graph.cool/simple/v1/cixmkt2ul01q00122mksg82pn'
        : 'https://localhost:8000/',
    },
    cms: {
      path: isProd
        ? 'https://api.graph.cool/simple/v1/cixmkt2ul01q00122mksg82pn'
        : 'https://localhost:8000/',
    },
  },
};

module.exports = appConfig;
