const appConfig = require('./app');

const aliasConfig = {
  ASSETS_PATH: appConfig.app.assets,
};

module.exports = aliasConfig;
