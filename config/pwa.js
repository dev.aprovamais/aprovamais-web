const pwaConfig = {
  swDest: './service-worker.js',
  runtimeCaching: [
    {
      urlPattern: /\.(?:png|jpg|jpeg|svg)$/,
      handler: 'CacheFirst',
      options: {
        cacheName: '/static',
        expiration: {
          maxEntries: 30,
        },
      },
    },
  ],
};

module.exports = pwaConfig;
